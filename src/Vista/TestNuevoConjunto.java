/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Util.Conjunto;

/**
 *
 * @author MADARME
 */
public class TestNuevoConjunto {

    public static void llenarConjuntoEnteros(Conjunto<Integer> c) throws Exception {
        for (int i = 0; i < c.getLength(); i++) {
            Integer numeroAgregar = (int) (Math.random() * 15 * 44 / 3);
            if (c.existeElemento(numeroAgregar))//Valido si el numero que me genera el metodo Math.radom ya existe                              
            {
                numeroAgregar = (int) (Math.random() * 2247 / 2);
            } else if (c.existeElemento(numeroAgregar)) {
                numeroAgregar = (int) (Math.random() * 227);

            } else if (c.existeElemento(numeroAgregar)) {
                numeroAgregar = (int) (Math.random() * 2272);
            }
            c.adicionarElemento(numeroAgregar);

        }

    }

    public static void mostrarOrdenamientoInserccion(Conjunto<Integer> c) {
        try {
            
            c.ordenar();            
            System.out.println(c.toString() + "(Ordenado por insercción)");
            

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void mostrarOrdenamientoBurbuja(Conjunto<Integer> c) {
        try {
            
            c.ordenarBurbuja();
            System.out.println(c.toString() + "(Ordenado por método burbuja)");
            
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void mostrarConjunto(Conjunto<Integer> s) {
        try {

            System.out.println("\nTamanio=" + s.getCajas().length + "\n" + s.toString());

        } catch (Exception e) {

            System.out.println(e.getMessage());

        }
    }
    // Nota: Tengo un metodo que me llena automaticamente con numeros aleatorios un arreglo de cajas de conjunto 
    // "llenarConjuntoEnteros(c1)", pero prefiero tener control de los elementos
    //que agrego para despues probar el metodo remover(), indexOf(), existeElemento().

    public static void main(String[] args) throws Exception {
        try {
            int pruebaExisteNumero = 390;
            int pruebaEliminarNumero = 356;
            
            Conjunto<Integer> c1 = new Conjunto(6);
            Conjunto<Integer> c2 = new Conjunto(16);
            Conjunto<Integer> c3 = new Conjunto(14);
            
            Conjunto<String> c4=new Conjunto(3);
            Conjunto<String> c5=new Conjunto(4);
            
            
            c4.adicionarElemento("Lucas");
            c4.adicionarElemento("Pedro");
            c4.adicionarElemento("Juan");
            
            c5.adicionarElemento("Maria");
            c5.adicionarElemento("Dina");
            c5.adicionarElemento("Raquel");
            c5.adicionarElemento("Yolima");
            
            c1.adicionarElemento(pruebaExisteNumero);
            c1.adicionarElemento(22);
            c1.adicionarElemento(347);
            c1.adicionarElemento(pruebaEliminarNumero);
            c1.adicionarElemento(534);
            c1.adicionarElemento(65);

            c2.adicionarElemento(23);
            c2.adicionarElemento(45);
            c2.adicionarElemento(100);
            c2.adicionarElemento(34);
            c2.adicionarElemento(29);
            c2.adicionarElemento(24);
            c2.adicionarElemento(pruebaExisteNumero);
            c2.adicionarElemento(67);
            c2.adicionarElemento(50);
            c2.adicionarElemento(65);
            c2.adicionarElemento(pruebaEliminarNumero);
            c2.adicionarElemento(44);
            c2.adicionarElemento(49);
            c2.adicionarElemento(54);
            c2.adicionarElemento(634);
            c2.adicionarElemento(77);

            c3.adicionarElemento(31);
            c3.adicionarElemento(415);
            c3.adicionarElemento(pruebaExisteNumero);
            c3.adicionarElemento(61);
            c3.adicionarElemento(71);
            c3.adicionarElemento(pruebaEliminarNumero);
            c3.adicionarElemento(51);
            c3.adicionarElemento(28);
            c3.adicionarElemento(635);
            c3.adicionarElemento(784);
            c3.adicionarElemento(85);
            c3.adicionarElemento(94);
            c3.adicionarElemento(514);
            c3.adicionarElemento(445);
//        
            System.out.println("===========================================================");
            System.out.println("Conjunto Varones");
            System.out.println(c4.toString());
            
            System.out.println("");
            System.out.println("Conjunto Mujeres");
            System.out.println(c5.toString());    
            
            System.out.println("===========================================================");
            System.out.println("Remover elemento Dina del conjunto Mujeres");
            c5.remover("Dina");
            System.out.println(c5.toString()); 
            System.out.println("===========================================================");
            
            System.out.println("Remover elemento Juan del conjunto Varones");
            c4.remover("Juan");
            System.out.println(c4.toString());
            System.out.println("===========================================================");
            System.out.println("Conjunto Varones ordenado por inserccion");
            c4.ordenar();
            System.out.println(c4.toString());
            System.out.println("===========================================================");
            System.out.println("Conjunto Mujeres ordenado por metodo burbuja");
            c5.ordenar();
            System.out.println(c5.toString());
            
            System.out.println("===========================================================");
            System.out.print("Conjunto 1 ");
            mostrarConjunto(c1);
            mostrarOrdenamientoInserccion(c1);//Metodo inserccion
            
            
            System.out.println("===========================================================");
            
            System.out.println("Conjunto 2 ");
            mostrarConjunto(c2);
            mostrarOrdenamientoInserccion(c2);
            System.out.println("===========================================================");  
            System.out.print("Conjunto 3 ");
            mostrarConjunto(c3); //Metodo burbuja
            mostrarOrdenamientoBurbuja(c3);
            System.out.println("===========================================================");
            System.out.println(""); ///Metodo existe
            System.out.println("¿Existe el elemento " + pruebaExisteNumero + " en el conjunto c1?= " + c1.existeElemento(pruebaExisteNumero));
            System.out.println("¿Existe el elemento " + pruebaExisteNumero + " en el conjunto c2?= " + c2.existeElemento(pruebaExisteNumero));
            System.out.println("¿Existe el elemento " + pruebaExisteNumero + " en el conjunto c3?= " + c3.existeElemento(pruebaExisteNumero));
            try {
            System.out.println("===========================================================");
            c1.remover(pruebaEliminarNumero); ///Metodo remover que compacta un conjunto
            System.out.println(" Conjunto 1 con elemento " + pruebaEliminarNumero + " eliminado\n");
            mostrarConjunto(c1);
            System.out.println("===========================================================");
            c2.remover(pruebaEliminarNumero);
            System.out.println(" Conjunto 2 con elemento " + pruebaEliminarNumero + " eliminado\n");
            mostrarConjunto(c2);
                
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

            System.out.println("===========================================================");
            
            System.out.println("La posicion del elemento "+pruebaExisteNumero+" en el conjunto 1 es "+c1.indexOf(pruebaExisteNumero));
            System.out.println("La posicion del elemento "+pruebaExisteNumero+" en el conjunto 2 es "+c2.indexOf(pruebaExisteNumero));
            System.out.println("La posicion del elemento "+pruebaExisteNumero+" en el conjunto 3 es "+c3.indexOf(pruebaExisteNumero));


            System.out.println("===========================================================");
            
            
            System.out.println("===========================================================");
            System.out.println();
            System.out.print("Conjunto 2 concatenado restrictivamente con 1\n");     
            System.out.println("");
            c2.concatenarRestrictivo(c1); ///Metodo concatenar restrictivo
            mostrarConjunto(c2);
            
            System.out.println("===========================================================");
            
            System.out.print("Conjunto 2 concatenado con 3 \n");
            
            c2.concatenar(c3); ///Metodo concatenar normal
            mostrarConjunto(c2);
            System.out.println("===========================================================");
        } catch (Exception e) 
        {
            System.out.println(e.getMessage());

        }
    }
}
